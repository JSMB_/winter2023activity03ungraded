public class Application {
	public static void main(String[] args) {
		Student st = new Student();
		
		Student brad = new Student();
		brad.name = "Brad";
		brad.grade = 'C';
		brad.sNumber = 433567;
		
		Student emily = new Student();
		emily.name = "Emily";
		emily.grade = 'A';
		emily.sNumber = 666666;
		
		Student[] section3 = new Student[3];
		section3[0] = brad;
		section3[1] = emily;
		
		section3[2] = new Student();
		
		section3[2].name = "Secret";
		section3[2].grade = 'S';
		section3[2].sNumber = 111110;
		
		System.out.println(section3[2].name);
		System.out.println(section3[2].grade);
		System.out.println(section3[2].sNumber);
		
		/*System.out.println(brad.name);
		System.out.println(brad.grade);
		System.out.println(brad.sNumber);
		
		System.out.println(emily.name);
		System.out.println(emily.grade);
		System.out.println(emily.sNumber);*/
		
		//This one is the incorrect way of calling the non-static(instance) method
		//Student.welcomeToStudent(brad.name);
		
		//This is the correct way to do it
		/*st.welcomeToStudent(brad.name);
		st.welcomeToStudent(emily.name);*/
	}
}